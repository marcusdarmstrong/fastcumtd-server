var express = require('express');
var request = require('request');
var MongoClient = require('mongodb').MongoClient;
var GCM = require('gcm').GCM;

var app = express();

var cumtdAPIConfig = {
    'root': "https://developer.cumtd.com/api/",
    'key': "83c90bdbf61c4a42abb56803bcf57055",
    'version': "v2.2",
    'format': "json"
}

var directionsAPIConfig = {
    'root': "https://maps.googleapis.com/maps/api/directions",
    'key': "AIzaSyBPs-td2MXlWA8KLnz64S0EdrLIwXWzeoQ",
    'format': "json"
};

var geocodeAPIConfig = {
    'root': "https://maps.googleapis.com/maps/api/geocode",
    'key': "AIzaSyBPs-td2MXlWA8KLnz64S0EdrLIwXWzeoQ",
    'format': "json"
};

var gcm = new GCM("AIzaSyBPs-td2MXlWA8KLnz64S0EdrLIwXWzeoQ");

var mongodbUrl = 'mongodb://heroku_app34189268:99slpq418rhgitui9kp2o4s8fs@ds061711.mongolab.com:61711/heroku_app34189268';

var buildAPIRequest = function(config, method, args) {
    var query = "";
    for (var key in args) {
        query = query + "&" + key + "=" + args[key];
    }
    return config.root + config.version + "/" + config.format + "/" + method + "?key=" + config.key + query;
};

var buildGoogleAPIRequest = function(config, args) {
    var query = "";
    for (var key in args) {
        query = query + "&" + key + "=" + args[key];
    }
    return config.root + "/" + config.format + "?key=" + config.key + query;
};

var removeContainer = function(response) {
    var data = response.calendar_dates
        || response.departures
        || response.reroutes
        || response.routes
        || response.shapes
        || response.stops
        || response.itineraries
        || response.stop_times
        || response.trips
        || response.vehicles
        || response.results
        || response;
    return data;
}

var doCache = function(method) {
    return method == "GetStops";
};

var stopsCache = "";

var haveCache = function() {
    return getCache() != "";
}

var getCache = function() {
    return stopsCache;
};

var addCache = function(method, args, result) {
    stopsCache = result;
}

var doAPIRequest = function(config, method, args, cb) {
    if (doCache(method, args) && haveCache(method, args)) {
        cb(getCache(method, args));
        return;
    }
    request(buildAPIRequest(config, method, args), function (error, response, body) {
        if (error) {
            console.log(JSON.stringify(error));
        }
        if (doCache(method, args)) {
            addCache(method, args, body);
        }
        cb(body);
    });
};

app.set('port', (process.env.PORT || 5000));

app.get('/:method', function(request, response) {
    doAPIRequest(cumtdAPIConfig, request.params.method, request.query, function(body) {
        response.send(JSON.stringify(removeContainer(JSON.parse(body))));
    });
});

app.get('/maps/directions', function(request, response) {
    buildGoogleAPIRequest(directionsAPIConfig, request.query, function(body) {
        response.send(JSON.stringify(removeContainer(JSON.parse(body))));
    });
});

app.get('/maps/geocode', function(request, response) {
    buildGoogleAPIRequest(geocodeAPIConfig, request.query, function(body) {
        response.send(JSON.stringify(removeContainer(JSON.parse(body))));
    });
});

app.get('/subscriptions/add', function(request, response){
    MongoClient.connect(mongodbUrl, function(err, db) {
        if (err != null) {
            response.send('{"status":"error"}');
            return;
        }

        db.collection('subscriptions').insert({
            "hour": request.query.hour,
            "minute": request.query.minute,
            "stop_id": request.query.stop_id,
            "registration_id": request.query.registration_id
        });
        response.send('{"status":"success"}');
    });
});

app.get('/subscriptions/get', function(request, response){
    MongoClient.connect(mongodbUrl, function(err, db) {
        if (err != null) {
            response.send('{"status":"error"}');
            return;
        }

        db.collection('subscriptions').find({
            "registration_id": request.query.registration_id
        }).toArray(function(err, docs){
            response.send(JSON.stringify(docs));
        });
    });
});

app.get('/subscriptions/remove', function(request, response){
    MongoClient.connect(mongodbUrl, function(err, db) {
        if (err != null) {
            response.send('{"status":"error"}');
            return;
        }

        db.collection('subscriptions').remove({
            "hour": request.query.hour,
            "minute": request.query.minute,
            "stop_id": request.query.stop_id,
            "registration_id": request.query.registration_id
        });
        response.send('{"status":"success"}');
    });
});

app.listen(app.get('port'), function() {
    console.log("FastCUmtd server is running at localhost:" + app.get('port'));
});

setInterval(function() {
    // Push messages.
    // First, let's generate messages...
    MongoClient.connect(mongodbUrl, function(err, db) {
        if (err != null) {
            return;
        }

        var currentDate = new Date;
        var currentHour = currentDate.getHours();
        var currentMinute = currentDate.getMinutes();
        if (currentMinute % 10 >= 5) {
            currentMinute = 5
        } else {
            currentMinute = 0
        }

        /* Schema for subscriptions {
            hour: 7,
            minute: 10,
            stop_id: "IU:2",
            registration_id: "blah"
        } */ 
        var subscriptions = db.collection('subscriptions');
        subscriptions.find({hour: currentHour, minute: currentMinute})
            .toArray(function(err, docs){
            for (var i in docs) {
                var doc = docs[i];
                doAPIRequest(cumtdAPIConfig, "GetDeparturesByStop", {"stop_id": doc.stop_id}, function(body) {
                    var departures = removeContainer(JSON.parse(body));
                    var message = "Upcoming departures -- "
                    for (var j in departures) {
                        var departure = departures[j];
                        message = message + departure.headsign + ": " + departure.expected_mins + " minutes";
                    }
                    gcm.send({
                        'registration_id': doc.registration_id,
                        'message': message
                    }, function(err, messageId){});
                });
            }
        });
    });
}, 5 * 60 * 1000);
